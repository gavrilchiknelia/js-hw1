"use strict";

let userName = prompt('Enter your name:', '');

function checkName(name) {

        while (name === '' || isNaN(parseInt(name)) === false || name === undefined){
           name =  prompt('Enter your name again:', '');
        }
        return name;
    }
userName = checkName(userName)

let contentDiv;
contentDiv = document.getElementById('mainContent');

let age = prompt('How old are you?', '');

function checkAge(userAge, userName) {

    while (!userAge.trim() || isNaN(parseInt(userAge)) || userAge < 0 || userAge > 100) {
        userAge = prompt('Enter your age again', '');
    }
    if (userAge < 18) {
        contentDiv.innerText = 'You are not allowed to visit this website';
    }
    else if (userAge > 22) {
        contentDiv.innerText = (`Welcome, ${userName}`);
    }
    else if (userAge >= 18 && userAge <= 22) {
        let pressOk = confirm('Are you sure you want to continue?');
        if (pressOk === true) {
            contentDiv.innerText = (`Welcome, ${userName}`);
        } else {
            contentDiv.innerText = 'You are not allowed to visit this website.';
        }
    }
}

checkAge(age, userName);
